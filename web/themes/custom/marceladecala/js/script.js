(function ($, Drupal) {
  Drupal.behaviors.mybehavior = {
    attach: function (context, settings) {

      $(window).on("load", function () {
        $(".se-pre-con").fadeOut("slow");
      });

      $(".homepage-slides .view-content", context).fullpage({
        navigation: true,
        navigationPosition: "right",
        showActiveTooltip: false,
        slidesNavigation: true,
        slidesNavPosition: "bottom",
        //Scrolling
        css3: true,
        scrollingSpeed: 1000,
        fitToSection: true,
        fitToSectionDelay: 1500,
        scrollBar: false,
        easing: "easeInOutCubic",
        easingcss3: "ease",
      });

      $('[data-fancybox="gallery"]', context).bind({
        caption: function (fancybox, carousel, slide) {
          return (
            `${slide.index + 1} / ${carousel.slides.length} <br />` +
            slide.caption
          );
        },
      });

      $("." + settings.path.currentLanguage, context).addClass("is-active");
    },
  };
})(jQuery, Drupal);
