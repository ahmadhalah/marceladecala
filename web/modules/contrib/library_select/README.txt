Library Select
=====================

Module allow quick and easy insert CSS/JS code and files to Drupal.
Library Select allow you create a field in your content type and Editor can
easy select predefined CSS/JS Code.


Configuration
=====================
Configuration URL: admin/config/development/library_select_entity


Context
=====================
There is sub module library_select_context integrate with Context module allow
you can easy attach library to any page.


===========================
This module borrow some code of module
Asset Injector: https://www.drupal.org/project/asset_injector
